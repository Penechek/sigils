%size(5*4). %X*Y where X is width and Y is height
%figuri([0,1,0,0,1,2,1]).
%figuri([2,0,2,0,0,0,1]).
size(8*5).
figuri([1,2,0,1,2,2,2]).
%size(7*4).
%figuri([1,0,1,0,0,2,3]).
%--------------------------------------------------
symboll(X,X).

generate2(_,0,[]):-!.
generate2(X,N,[X|List]):-
	N2 is N-1,
	generate2(X,N2,List).

generate(Elem,Result):-
	size(W*H),
	generate2(Elem,H,List),
	generate2(List,W,Result).


showlist([]):-nl,!.
showlist([X|L]):-
	write(X),write(' '),
	showlist(L).

%show([]):-nl,!.

show([X|L]):-
	showlist(X),
	show(L).


sigil(Matrix,Sigil,P,Spisok):-
	Spisok=[X1/Y1,X2/Y2,X3/Y3,X4/Y4],
	name(sig,L1),
	name(Sigil,L2),
	append(L1,L2,Res),
	name(Name,Res),
%sig+(1,2,3,4,5,6,7)(P,[X1/Y1,bla-bla]).
/*	size(W*H),
	numlist(1,W,List1),
	numlist(1,H,List2),
	member(X1,List1),
	member(Y1,List2),*/
        mynth1(0,X1,Y1,Matrix),
	Term=..[Name,P,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]],
	call(Term).
/*
sigil(2,P,Spisok):-
	sig2(P,Spisok),!.
sigil(5,P,Spisok):-
	sig5(P,Spisok),!.
sigil(6,P,Spisok):-
	sig6(P,Spisok),!.
sigil(7,P,Spisok):-
	sig7(P,Spisok),!.

*/

%------------------------Drawing =)-------------------
sig1(1,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1,Y2 is Y1+1,
	X3 is X1+1,Y3 is Y1,
	X4 is X1+1,Y4 is Y1+1.

sig2(1,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1+1,Y2 is Y1,
	X3 is X1,Y3 is Y1+1,
	X4 is X1,Y4 is Y1+2.

sig2(2,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1,Y2 is Y1+1,
	X3 is X1+1,Y3 is Y1+1,
	X4 is X1+2,Y4 is Y1+1.

sig2(3,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1+1,Y2 is Y1,
	X3 is X1+1,Y3 is Y1-1,
	X4 is X1+1,Y4 is Y1-2.

sig2(4,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1+1,Y2 is Y1,
	X3 is X1+2,Y3 is Y1,
	X4 is X1+2,Y4 is Y1+1.

sig3(1,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1,Y2 is Y1+1,
	X3 is X1,Y3 is Y1+2,
	X4 is X1+1,Y4 is Y1+2.
sig3(2,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1+1,Y2 is Y1,
	X3 is X1+2,Y3 is Y1,
	X4 is X1+2,Y4 is Y1-1.

sig3(3,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1+1,Y2 is Y1,
	X3 is X1+1,Y3 is Y1+1,
	X4 is X1+1,Y4 is Y1+2.

sig3(4,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1,Y2 is Y1+1,
	X3 is X1+1,Y3 is Y1,
	X4 is X1+2,Y4 is Y1.

sig4(1,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1,Y2 is Y1+1,
	X3 is X1+1,Y3 is Y1,
	X4 is X1+1,Y4 is Y1-1.

sig4(2,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1+1,Y2 is Y1,
	X3 is X1+1,Y3 is Y1+1,
	X4 is X1+2,Y4 is Y1+1.

sig5(1,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1,Y2 is Y1+1,
	X3 is X1+1,Y3 is Y1+1,
	X4 is X1+1,Y4 is Y1+2.

sig5(2,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1 +1,Y2 is Y1,
	X3 is X1+1,Y3 is Y1-1,
	X4 is X1+2,Y4 is Y1-1.

sig6(1,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1+1,X3 is X1+1,X4 is X1+1,
	Y2 is Y1-1,Y3 is Y1,Y4 is Y1+1.

sig6(2,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1+1,Y2 is Y1,
	X3 is X1+1,Y3 is Y1+1,
	X4 is X1+2,Y4 is Y1.

sig6(3,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1,Y2 is Y1+1,
	X3 is X1+1,Y3 is Y1+1,
	X4 is X1,Y4 is Y1+2.

sig6(4,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1+1,Y2 is Y1,
	X3 is X1+1,Y3 is Y1-1,
	X4 is X1+2,Y4 is Y1.

sig7(1,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	Y2 is Y1+1,Y3 is Y1+2,Y4 is Y1+3,
	X2 is X1,X3 is X1,X4 is X1.

sig7(2,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X2 is X1+1,X3 is X1+2,X4 is X1+3,
	Y2 is Y1,Y3 is Y1,Y4 is Y1.

govno(_,X,Y,[X1/Y1,X2/Y2,X3/Y3,X4/Y4]):-
	X1 is X-1,Y1 is Y,
	X2 is X,Y2 is Y+1,
	X3 is X+1,Y3 is Y,
	X4 is X,Y4 is Y-1.

govno(L,X,Y,[X1/Y1,X2/Y2,X3/Y3,X4/Y4,X5/Y5,X6/Y6]):-
	Yhui is Y+1,mynth1(0,X,Yhui,L),
	X1 is X-1,Y1 is Y,
	X2 is X-1,Y2 is Y+1,
	X3 is X,Y3 is Y+2,
	X4 is X+1,Y4 is Y+1,
	X5 is X+1,Y5 is Y,
	X6 is X,Y6 is Y-1.

govno(L,X,Y,[X1/Y1,X2/Y2,X3/Y3,X4/Y4,X5/Y5,X6/Y6]):-
	Xhui is X+1,mynth1(0,Xhui,Y,L),
	X1 is X-1,Y1 is Y,
	X2 is X,Y2 is Y+1,
	X3 is X+1,Y3 is Y+1,
	X4 is X+2,Y4 is Y,
	X5 is X+1,Y5 is Y-1,
	X6 is X,Y6 is Y-1.


govno(L,X,Y,[X1/Y1,X2/Y2,X3/Y3,X4/Y4,X5/Y5,X6/Y6,X7/Y7,X8/Y8]):-
	Yhui is Y+1,mynth1(0,X,Yhui,L),
	Yhui2 is Y+2,mynth1(0,X,Yhui2,L),
	X1 is X-1,Y1 is Y,
	X2 is X-1,Y2 is Y+1,
	X3 is X-1,Y3 is Y+2,
	X4 is X+1,Y4 is Y+1,
	X5 is X+1,Y5 is Y,
	X6 is X,Y6 is Y-1,
	X7 is X-1,Y7 is Y+2,
	X8 is X,Y8 is Y+3.


govno(L,X,Y,[X1/Y1,X2/Y2,X3/Y3,X4/Y4,X5/Y5,X6/Y6,X7/Y7,X8/Y8]):-
	Xhui is X+1,mynth1(0,Xhui,Y,L),
	Xhui2 is X+2,mynth1(0,Xhui2,Y,L),
	X1 is X-1,Y1 is Y,
	X2 is X,Y2 is Y+1,
	X3 is X+1,Y3 is Y+1,
	X4 is X+2,Y4 is Y+1,
	X5 is X+1,Y5 is Y-1,
	X6 is X,Y6 is Y-1,
	X7 is X+2,Y7 is Y-1,
	X8 is X+3,Y8 is Y.


%------------------------EndDrawing------------------
mynth1(Elem,X,Y,L):-
	nth1(X,L,Spis),
	nth1(Y,Spis,Elem).

verify(L,Spisok):-
	size(W*H),
	bagof(Elem1,Hui^member(Elem1/Hui,Spisok),ListX),
	bagof(Elem2,Hui2^member(Hui2/Elem2,Spisok),ListY),
	max_list(ListX,Xmax),min_list(ListX,Xmin),
	max_list(ListY,Ymax),min_list(ListY,Ymin),
	Xmin>=1,Xmax=<W,
	Ymin>=1,Ymax=<H,
	Spisok=[X1/Y1,X2/Y2,X3/Y3,X4/Y4],
	mynth1(0,X1,Y1,L),
	mynth1(0,X2,Y2,L),
	mynth1(0,X3,Y3,L),
	mynth1(0,X4,Y4,L),!.

dec([X|L],1,[X2|L]):-X2 is X-1,!.
dec([X|L],N,[X|L2]):-N2 is N-1,dec(L,N2,L2).

zamena([OldElem|L],OldElem,1,Elem,[Elem|L]):-!.
zamena([X|L],OldElem,Index,Elem,[X|L2]):-
	Index2 is Index-1,
	zamena(L,OldElem,Index2,Elem,L2).

izobr(_,L,[],L):-!.
izobr(N,L1,[X/Y|S],L2):-
       symboll(N,Sigil),
	zamena(L1,OldList,X,NewList,L3),
	zamena(OldList,_,Y,Sigil,NewList),
	izobr(N,L3,S,L2).

zapol(N,Coord,L1,S1,L2,S2):-
	dec(S1,N,S2),
	izobr(N,L1,Coord,L2).

/*pustota(L,N):-
	size(W*H),
	mynth1(0,X,Y,L),
	X1 is X-1,Y1 is Y,
	X2 is X,Y2 is Y+1,
	X3 is X+1,Y3 is Y,
	X4 is X,Y4 is Y-1,
	(X1<1;not(mynth1(0,X1,Y1,L))),
	(Y2>H;not(mynth1(0,X2,Y2,L))),
	(X3>W;not(mynth1(0,X3,Y3,L))),
	(Y4<1;not(mynth1(0,X4,Y4,L))),!,
	N is 999;N is 0.*/

proverka([],_):-!.
proverka([X/Y|L],Matrix):-
	not(mynth1(0,X,Y,Matrix)),
	proverka(L,Matrix),!.

zapret(L):-
	mynth1(0,X,Y,L),
	govno(L,X,Y,Coord),
	proverka(Coord,L),!,fail;true.
start(L-S):-generate(0,L),figuri(S).

%-----------------------Special predicates------------------------

s(L-S,L2-S2,1):-s2(L-S,L2-S2).
s2(L-S,L2-S2):-
	member(X,[1,2,3,4,5,6,7]),
	not(nth1(X,S,0)),
	member(P,[1,2,3,4]),
	sigil(L,X,P,Spisok),
	verify(L,Spisok),
	zapol(X,Spisok,L,S,L2,S2),
	zapret(L2).

h(_-S,N):-sum_list(S,N).

goal(_-L):-member(X,L),X=\=0,!,fail;true.
