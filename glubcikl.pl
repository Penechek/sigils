solve(Node,Sol):-depthfirst([],Node,Sol).
depthfirst(Path,Node,[Node|Path]):-goal(Node).
depthfirst(Path,Node,Sol):-
	s(Node,Node2),
	not(member(Node2,Path)),
	depthfirst([Node|Path],Node,Sol).


solve2(Node,Sol):-Node = _-S,sumlist(S,N),depthfirst2(Node,Sol,N),!.
depthfirst2(Node,[Node],_):-goal(Node),!.
depthfirst2(Node,[Node|Sol],Max):-
	Max>0,
	s2(Node,Node2),
	depthfirst2(Node2,Sol,Max-1).


%solve3(Node,Sol):-breadfirst([[Node]],Sol).

solve3( Start, Solution)  :-
  breadthfirst( [ [Start] ], Solution).

% breadthfirst( [ Path1, Path2, ...], Solution):
%   Solution is an extension to a goal of one of paths

breadthfirst( [ [Node | Path] | _], [Node | Path])  :-
  goal( Node).

breadthfirst( [Path | Paths], Solution)  :-
  extend( Path, NewPaths),
  append( Paths, NewPaths, Paths1),
  breadthfirst( Paths1, Solution).

extend( [Node | Path], NewPaths)  :-
  bagof( [NewNode, Node | Path],
         ( s2( Node, NewNode), not( member( NewNode, [Node | Path] ) )),
         NewPaths),
  !.

extend( Path, [] ).              % bagof failed: Node has no successor

